package org.nrg.xdat.shared;

import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatResourceseries;
import org.nrg.xft.exception.InvalidValueException;
import org.nrg.xft.utils.FileUtils;

import java.util.List;

public class OmUtils {
    private OmUtils() {
        // Nothing here.
    }

    public static void validateXnatAbstractResources(final String expectedPath, final List<XnatAbstractresourceI> resources) throws InvalidValueException {
        for (final XnatAbstractresourceI resource : resources) {
            final String uri;
            if (resource instanceof XnatResource) {
                uri = ((XnatResource) resource).getUri();
            } else if (resource instanceof XnatResourceseries) {
                uri = ((XnatResourceseries) resource).getPath();
            } else {
                continue;
            }
            FileUtils.ValidateUriAgainstRoot(uri, expectedPath, "URI references data outside of the project:" + uri);
        }
    }
}
